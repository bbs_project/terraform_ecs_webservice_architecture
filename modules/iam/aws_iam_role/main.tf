resource "aws_iam_role" "assume_role" {
  name               = "iam-role-${var.name}-tf"
  path               = var.role_path
  assume_role_policy = var.assume_role_json

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_policy" "policy" {
  name        = "iam-policy-${var.name}-tf"
  path        = var.policy_path
  description = var.description
  policy      = var.policy_document_json

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_role_policy_attachment" "policy_attach" {
  role       = aws_iam_role.assume_role.name
  policy_arn = aws_iam_policy.policy.arn

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_instance_profile" "iprofile" {
  name = "iam-role-${var.name}-tf"
  path = var.profile_path
  role = aws_iam_role.assume_role.name

  lifecycle {
    create_before_destroy = true
  }
}

