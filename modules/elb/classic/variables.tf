variable "azs" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "elb_name" {
  default = ""
}

variable "elb_port" {
  default = ""
}

variable "elb_protocol" {
  default = ""
}

variable "elb_instance_protocol" {
  default = ""
}

variable "elb_instance_port" {
  default = ""
}

variable "elb_ssl_port" {
  default = ""
}

variable "elb_ssl_protocol" {
  default = ""
}

variable "elb_ssl_instance_protocol" {
  default = ""
}

variable "elb_ssl_instance_port" {
  default = ""
}

variable "elb_ssl_certificate_id" {
  default = ""
}

variable "elb_subnet_ids" {
  default = ""
}

variable "elb_health_check" {
  default = ""
}

variable "http_allowed_ips" {
  default = ""
}

variable "ssh_allowed_ips" {
  default = ""
}

variable "health_check_healthy_threshold" {
  default = 2
}

variable "health_check_unhealthy_threshold" {
  default = 2
}

variable "health_check_timeout" {
  default = 3
}

variable "health_check_target" {
  default = "HTTP:80/"
}

variable "health_check_interval" {
  default = 10
}

variable "elb_instance_ids" {
  default = ""
}

variable "elb_cross_zone_load_balancing" {
  default = true
}

variable "elb_idle_timeout" {
  default = 400
}

variable "elb_connection_draining" {
  default = true
}

variable "elb_connection_draining_timeout" {
  default = 400
}

variable "aws_security_group_ids" {
  default = ""
}