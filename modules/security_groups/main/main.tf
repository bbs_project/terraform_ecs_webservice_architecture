#################
# Security group
#################

resource "aws_security_group" "main" {
  lifecycle {
    create_before_destroy = true
  }
  name        = var.main_name
  description = var.main_description
  vpc_id      = var.main_vpc_id

  tags = {
    Name = var.main_name
  }
}

###############
# Ingress rules
###############

resource "aws_security_group_rule" "ingress_rules" {
  lifecycle {
    create_before_destroy = true
  }
  count = length(var.map_security_ingress_rules)

  security_group_id = aws_security_group.main.id
  type              = "ingress"
  cidr_blocks = split(
    ",",
    lookup(
      var.map_security_ingress_rules[format("ingress_%d", count.index)],
      "main_cidr_blocks",
      "0.0.0.0/0",
    ),
  )
  description = lookup(
    var.map_security_ingress_rules[format("ingress_%d", count.index)],
    "main_description",
    "all",
  )
  from_port = lookup(
    var.map_security_ingress_rules[format("ingress_%d", count.index)],
    "main_from_port",
    "0",
  )
  to_port = lookup(
    var.map_security_ingress_rules[format("ingress_%d", count.index)],
    "main_to_port",
    "0",
  )
  protocol = lookup(
    var.map_security_ingress_rules[format("ingress_%d", count.index)],
    "main_protocol",
    "-1",
  )
}

##############
# Egress rules
##############

resource "aws_security_group_rule" "egress_rules" {
  lifecycle {
    create_before_destroy = true
  }
  count = length(var.map_security_egress_rules)

  security_group_id = aws_security_group.main.id
  type              = "egress"
  cidr_blocks = split(
    ",",
    lookup(
      var.map_security_egress_rules[format("egress_%d", count.index)],
      "main_cidr_blocks",
      "0.0.0.0/0",
    ),
  )
  description = lookup(
    var.map_security_egress_rules[format("egress_%d", count.index)],
    "main_description",
    "all",
  )
  from_port = lookup(
    var.map_security_egress_rules[format("egress_%d", count.index)],
    "main_from_port",
    "0",
  )
  to_port = lookup(
    var.map_security_egress_rules[format("egress_%d", count.index)],
    "main_to_port",
    "0",
  )
  protocol = lookup(
    var.map_security_egress_rules[format("egress_%d", count.index)],
    "main_protocol",
    "-1",
  )
}

