variable "main_security_group" {
  description = "Control if create security group"
  default     = true
}

variable "main_name" {
  description = "Name of security group"
  default     = ""
}

variable "main_description" {
  description = "Description of security group"
  default     = "Security Group managed by Terraform"
}

variable "main_vpc_id" {
  description = "ID of the VPC where to create security group"
}

variable "map_security_ingress_rules" {
  default = {}
}

variable "map_security_egress_rules" {
  default = {}
}

