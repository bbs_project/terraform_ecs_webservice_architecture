resource "aws_instance" "web" {
  lifecycle {
    ignore_changes = [ebs_block_device,user_data_base64]
  }
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = element(split(",", var.subnet_id), 0)
  associate_public_ip_address = var.associate_public_ip_address
  vpc_security_group_ids      = split(",", var.vpc_security_group_ids)
  key_name                    = var.key_name
  iam_instance_profile        = var.iam_instance_profile
  source_dest_check           = var.source_dest_check
  user_data_base64            = var.user_data_base64
  ebs_optimized               = var.ebs_optimized
  ebs_block_device {
    device_name           = var.ebs_device_name
    volume_type           = var.ebs_volume_type
    volume_size           = var.ebs_volume_size
    delete_on_termination = var.ebs_delete_on_termination
    encrypted             = var.ebs_encrypted
  }

  credit_specification {
    cpu_credits = var.cpu_credits
  }

  volume_tags = merge(
    {
      "Name" = var.instance_name
    },
    var.instance_tags,
  )
  tags = merge(
    {
      "Name" = var.instance_name
    },
    var.instance_tags,
  )
}

resource "aws_eip" "web" {
  count    = var.associate_public_ip_address ? 1 : 0
  instance = aws_instance.web.id
  vpc      = true
  tags = merge(
    {
      "Name" = var.instance_name
    },
    var.ip_instance_tags,
  )
}

