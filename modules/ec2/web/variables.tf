variable "ami" {
  default = ""
}

variable "instance_type" {
  default = ""
}

variable "instance_name" {
  default = "instance_name"
}

variable "instance_tags" {
  default = {}
}

variable "subnet_id" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "ebs_device_name" {
  default = "/dev/xvda"
}

variable "ebs_volume_type" {
  default = "gp2"
}

variable "ebs_volume_size" {
  default = "30"
}

variable "ebs_delete_on_termination" {
  default = true
}

variable "ebs_encrypted" {
  default = false
}

variable "iam_instance_profile" {
  default = ""
}

variable "source_dest_check" {
  default = "true"
}

variable "user_data_base64" {
  default = ""
}

variable "ebs_optimized" {
  default = "true"
}

variable "associate_public_ip_address" {
  default = true
}

variable "vpc_security_group_ids" {
}

variable "ip_instance_tags" {
  default = {}
}

variable "cpu_credits" {
  default = "unlimited"
}

