# Setup key pair resources

- key_name

## Variables
#### Customize filed which use to config a new resource

- key_name: The customize name for the created key
- public_key: Public key of open SSH

## Outputs
#### Output parameters of the created resource which will be printed on the console

- key_name: Name of created key pair resource
