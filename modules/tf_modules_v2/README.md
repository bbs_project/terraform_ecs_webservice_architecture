# Terraform Modules

[terraform]: https://terraform.io

[Terraform][terraform] modules use for configuring infrastructure with AWS.


Terraform Modules includes :

- asg auto-scaling group of instances
- vpc with different subnets for availability
- elb definition for each service
- bastion node for manual SSH access
- rds aurora and mysql for database

## Preparing

_To use Terraform Modules, you'll need AWS access and Terraform installed, check out the [requirements](#requirements) section._

Get remote modules

    $ terraform get

Check which will stage the change set

    $ terraform plan

 Apply them to our infrastructure

    $ terraform apply


Now that we've got all the basics setup, how about adding a service?

Here's a sample service definition, try adding it to your `terraform.tf` file.

```hcl
# frontend elb
module "frontend_elb" {
  source = "git::ssh://git@tool.devsep.com:7999/infra/tf_modules//elb"

  azs                                 = "${var.azs}"
  vpc_id                              = "${module.vpc.vpc_id}"
  elb_name                            = "${var.frontend_elb_name}"
  elb_subnet_ids                      = "${module.vpc.public_subnet_ids}"
  elb_security_group_name             = "${var.frontend_elb_security_group_name}"
  http_allowed_ips                    = "${var.http_allowed_ips}"
  ssh_allowed_ips                     = "${var.ssh_allowed_ips}"
  elb_port                            = "${var.frontend_elb_port}"
  elb_protocol                        = "${var.frontend_elb_protocol}"
  elb_instance_port                   = "${var.frontend_elb_instance_port}"
  elb_instance_protocol               = "${var.frontend_elb_instance_protocol}"
}
```

Once the elb module has been added, simply run get, plan and apply:

    $ terraform get
    $ terraform plan
    $ terraform apply

## Requirements

Before we start, we will first need:

- [ ] an [AWS account][aws] with API access
- [ ] to [create a keypair][keypair] in AWS
- [ ] download and install [terraform][terraform]
