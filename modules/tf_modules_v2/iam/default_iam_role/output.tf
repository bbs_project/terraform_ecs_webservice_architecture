output "iam_role_name" {
  value = "${aws_iam_role.assume_role.name}"
}

output "iam_role_arn" {
  value = "${aws_iam_role.assume_role.arn}"
}