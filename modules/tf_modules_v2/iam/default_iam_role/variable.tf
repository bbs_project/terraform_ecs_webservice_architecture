variable "name" {
  description = "Unique name for an individual IAM Role."
}

variable "description" {
  description = "AWS Resource description"
}

variable "assume_role_json" {
  description = "Role policy to be assumed by an AWS resource. Defined as a data source."
}

variable "policy_arn_ids" {
  description = "List of policy arn"
}

variable "role_path" {
  default = "/"
}

variable "profile_path" {
  default = "/"
}

