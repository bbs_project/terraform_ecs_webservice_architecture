# Create iam role with default policy (AWS policy)

## How to use:

### terraform.tfvar
```
env = "dev"
aws_batch_service_role_name = "aws-batch-service-role"
aws_batch_service_role_path = "/service-role/"
policy_arn_ids              = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole,arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
```
### main.tf
```
data "aws_iam_policy_document" "aws_batch_service_role" {
	statement {
		effect  = "Allow"
		actions = ["sts:AssumeRole"]

		principals {
  			type        = "Service"
  			identifiers = ["batch.amazonaws.com"]
		}
	}
}

module "aws_batch_service_role" {
    source = "<<PATH_TO_MODULE>>"
    name                  = "${var.env}-${var.aws_batch_service_role_name}"
    assume_role_json      = "${data.aws_iam_policy_document.aws_batch_service_role.json}"
    role_path             = "${var.aws_batch_service_role_path}"
    policy_arn_ids        = "${var.policy_arn_ids}"
    description           = "${var.env}-${var.aws_batch_service_role_name}"
}
```