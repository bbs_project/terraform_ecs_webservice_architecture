resource "aws_iam_role" "assume_role" {
  name               = "iam-role-${var.name}-tf"
  path               = "${var.role_path}"
  assume_role_policy = "${var.assume_role_json}"

  lifecycle {
        create_before_destroy = true
      }

}

resource "aws_iam_role_policy_attachment" "policy_attach" {
  count      = "${length(split(",",var.policy_arn_ids))}"
  role       = "${aws_iam_role.assume_role.name}"
  policy_arn =  "${element(split(",",var.policy_arn_ids),count.index)}"
#   policy_arn = "${element(var.policy_arn_ids,count.index)}"

  lifecycle {
        create_before_destroy = true
      }

}

resource "aws_iam_instance_profile" "iprofile" {
  name = "iam-role-${var.name}-tf"
  path = "${var.profile_path}"
  role = "${aws_iam_role.assume_role.name}"

  lifecycle {
        create_before_destroy = true
      }

}