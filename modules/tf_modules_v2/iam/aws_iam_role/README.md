# Create iam role with custom policy

## How to use:

### terraform.tfvar
```
env = "dev"
ecs_instance_role_name = "ecs_instance_role_name"
```

### main.tf
```
data "aws_iam_policy_document" "ecs_instance_role" {
	statement {
		effect  = "Allow"
		actions = ["sts:AssumeRole"]

		principals {
  			type        = "Service"
  			identifiers = ["ec2.amazonaws.com"]
		}
	}
}

data "aws_iam_policy_document" "ecs_instance_role_policy" {
	statement {
		effect = "Allow"

		actions = [
  			    "ec2:DescribeTags",
                "ecs:CreateCluster",
                "ecs:DeregisterContainerInstance",
                "ecs:DiscoverPollEndpoint",
                "ecs:Poll",
                "ecs:RegisterContainerInstance",
                "ecs:StartTelemetrySession",
                "ecs:UpdateContainerInstancesState",
                "ecs:Submit*",
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
		]

		resources = ["*"]
	}
}



module "ecs_instance_role" {
    source = "<<PATH_TO_MODULE>>"
    name                  = "${var.env}-${var.ecs_instance_role_name}"
    assume_role_json      = "${data.aws_iam_policy_document.ecs_instance_role.json}"
    policy_document_json  = "${data.aws_iam_policy_document.ecs_instance_role_policy.json}"
    description           = "${var.env}-${var.ecs_instance_role_name}"
```