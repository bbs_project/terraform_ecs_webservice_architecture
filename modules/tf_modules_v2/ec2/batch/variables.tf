variable "batch_ami_id" {
  default = ""
}

variable "batch_instance_type" {
  default = ""
}

variable "batch_instance_name" {
  default = ""
}

variable "batch_instance_tags" {
  default = {}
}


variable "batch_subnet_id" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "batch_security_group_name" {
  default = ""
}

variable "ssh_allowed_ips" {
  default = ""
}

variable "ssh_allowed_sgs" {
  default = ""
}

variable "batch_associate_public_ip_address" {
  default = true
}

variable "batch_ebs_device_name" {
  default = "/dev/xvda"
}

variable "batch_ebs_volume_type" {
  default = "gp2"
}

variable "batch_ebs_volume_size" {
  default = "30"
}

variable "batch_ebs_delete_on_termination" {
  default = true
}

variable "batch_ebs_encrypted" {
  default = false
}

variable "batch_iam_instance_profile" {
  default = ""
}

variable "cpu_credits" {
  default = "standard"
}
