resource "aws_instance" "batch" {
  lifecycle {
    ignore_changes = ["ebs_block_device"]
  }
  ami                                 = "${var.batch_ami_id}"
  instance_type                       = "${var.batch_instance_type}"

  volume_tags                         = "${merge(map("Name", "${var.batch_instance_name}"), var.batch_instance_tags)}"
  tags                                = "${merge(map("Name", "${var.batch_instance_name}"), var.batch_instance_tags)}"

  subnet_id                           = "${element(split(",", var.batch_subnet_id), count.index)}"
  associate_public_ip_address         = "${var.batch_associate_public_ip_address}"
  vpc_security_group_ids              = ["${aws_security_group.batch.id}"]
  key_name                            = "${var.key_name}"
  iam_instance_profile                = "${var.batch_iam_instance_profile}"
  ebs_block_device = {
    device_name = "${var.batch_ebs_device_name}"
    volume_type = "${var.batch_ebs_volume_type}"
    volume_size = "${var.batch_ebs_volume_size}"
    delete_on_termination = "${var.batch_ebs_delete_on_termination}"
    encrypted = "${var.batch_ebs_encrypted}"
  }
  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }
}
