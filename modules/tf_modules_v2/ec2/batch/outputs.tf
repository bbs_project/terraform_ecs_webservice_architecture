output "batch_security_group_id" {
  value = "${aws_security_group.batch.id}"
}

output "batch_instance_id" {
  value = "${aws_instance.batch.id}"
}
