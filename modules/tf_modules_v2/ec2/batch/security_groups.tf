resource "aws_security_group" "batch" {
  lifecycle { create_before_destroy   = true }
  name                                = "${var.batch_security_group_name}"
  description                         = "batch security groups"
  vpc_id                              = "${var.vpc_id}"
  tags {
    Name                              = "${var.batch_security_group_name}"
  }
  # SSH access from IP, Security Group
  ingress {
    from_port                         = 22
    to_port                           = 22
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.ssh_allowed_ips)}"]
  }

  ingress {
    from_port                         = 22
    to_port                           = 22
    protocol                          = "tcp"
    security_groups                   = ["${split(",", var.ssh_allowed_sgs)}"]
  }

  # outbound internet access
  egress {
    from_port                         = 0
    to_port                           = 0
    protocol                          = "-1"
    cidr_blocks                       = ["0.0.0.0/0"]
  }
}
