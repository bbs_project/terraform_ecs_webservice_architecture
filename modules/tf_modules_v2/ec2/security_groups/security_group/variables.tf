variable "vpc_id" {
  default = ""
}

variable "port" {
  default = ""
}

variable "security_group_name" {
  default = ""
}

variable "allowed_ips" {
  default = ""
}
