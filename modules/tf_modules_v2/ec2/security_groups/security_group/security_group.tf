resource "aws_security_group" "main" {
  lifecycle { create_before_destroy   = true }
  name                                = "${var.security_group_name}"
  description                         = "security groups"
  vpc_id                              = "${var.vpc_id}"
  tags {
    Name                              = "${var.security_group_name}"
  }

  ingress {
    from_port                         = "${var.port}"
    to_port                           = "${var.port}"
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.allowed_ips)}"]
  }
}
