variable "vpc_id" {
  default = ""
}

variable "base_security_group_name" {
  default = ""
}

variable "ssh_allowed_ips" {
  default = ""
}

variable "http_allowed_ips" {
  default = ""
}
