resource "aws_security_group" "base" {
  lifecycle { create_before_destroy   = true }
  name                                = "${var.base_security_group_name}"
  description                         = "base security groups"
  vpc_id                              = "${var.vpc_id}"
  tags {
    Name                              = "${var.base_security_group_name}"
  }
  # HTTP/HTTPs access
  ingress {
    from_port                         = 80
    to_port                           = 80
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.http_allowed_ips)}"]
  }

  ingress {
    from_port                         = 443
    to_port                           = 443
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.http_allowed_ips)}"]
  }

  ingress {
    from_port                         = 22
    to_port                           = 22
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.ssh_allowed_ips)}"]
  }
  # outbound internet access
  egress {
    from_port                         = 0
    to_port                           = 0
    protocol                          = "-1"
    cidr_blocks                       = ["0.0.0.0/0"]
  }
}
