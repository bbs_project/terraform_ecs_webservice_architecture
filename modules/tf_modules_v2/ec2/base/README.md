# Setup EC2 resources

- EC2 instance

## Variables
#### Customize filed which use to config a new resource

- base_ami_id: The ID of the AMI used to launch the instance
- base_instance_type: The type of the Instance
- base_instance_name: The customize name of EC2 instance
- vpc_id: The VPC ID
- base_subnet_id: The VPC subnet ID
- key_name: The key name of the Instance
- base_security_group_name: The name of associated security group in a non-default VPC
- base_ebs_device_name: The physical name of the device
- base_ebs_volume_type: The volume type
- base_ebs_volume_size: The size of the volume, in GiB
- base_ebs_delete_on_termination: If the EBS volume will be deleted on termination
- base_ebs_encrypted: If the EBS volume is encrypted

## Outputs
#### Output parameters of the created resource which will be printed on the console

- base_instance_id: ID of created EC2 instance
