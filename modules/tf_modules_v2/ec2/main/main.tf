resource "aws_instance" "base" {
  lifecycle {
    ignore_changes = ["ebs_block_device"]
  }
  ami                                 = "${var.base_ami_id}"
  instance_type                       = "${var.base_instance_type}"
  tags = {
    Name                              = "${var.base_instance_name}"
  }
  volume_tags = {
    Name                              = "${var.base_instance_name}"
  }

  volume_tags                         = "${merge(map("Name", "${var.base_instance_name}"), var.base_instance_tags)}"
  tags                                = "${merge(map("Name", "${var.base_instance_name}"), var.base_instance_tags)}"

  subnet_id                           = "${element(split(",", var.base_subnet_id), count.index)}"
  associate_public_ip_address         = "${var.associate_public_ip_address}"
  vpc_security_group_ids              = ["${split(",", var.vpc_security_group_ids)}"]
  key_name                            = "${var.key_name}"
  iam_instance_profile                = "${var.base_iam_instance_profile}"
  ebs_block_device = {
    device_name = "${var.base_ebs_device_name}"
    volume_type = "${var.base_ebs_volume_type}"
    volume_size = "${var.base_ebs_volume_size}"
    delete_on_termination = "${var.base_ebs_delete_on_termination}"
    encrypted = "${var.base_ebs_encrypted}"
  }
  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }
}
