## ec2 main module

### input variables
```
#ami
default : ""

#instance_type
default : ""

#instance_name
default : ""

#subnet_id
default : ""

#associate_public_ip_address
default : ""

#vpc_security_group_ids
default : ""

#key_name
default : ""

#ebs_block_device_device_name
default : ""

#ebs_block_device_volume_type
default : ""

#ebs_block_device_volume_size
default : ""

#ebs_block_device_delete_on_termination
default : ""

#ebs_block_device_encrypted
default : ""

```

### output
```
#main_instance_id
```

### how to use
```
module "aws_instance_main" {
  source = "git::ssh://git@tool.devsep.com:7999/infra/tf_modules_v2//ec2/main"
  ami                                    = "${var.ami}"
  instance_type                          = "${var.instance_type}"
  instance_name                          = "${var.instance_name}"
  subnet_id                              = "${var.subnet_id}"
  associate_public_ip_address            = "${var.associate_public_ip_address}"
  vpc_security_group_ids                 = "${var.vpc_security_group_ids}"
  key_name                               = "${var.key_name}"
  ebs_block_device_device_name           = "${var.ebs_block_device_device_name}"
  ebs_block_device_volume_type           = "${var.ebs_block_device_volume_type}"
  ebs_block_device_volume_size           = "${var.ebs_block_device_volume_size}"
  ebs_block_device_delete_on_termination = "${var.ebs_block_device_delete_on_termination}"
  ebs_block_device_encrypted             = "${var.ebs_block_device_encrypted}"
}
```

### references
https://www.terraform.io/docs/providers/aws/r/instance.html
