# ec2 main module variables

variable "base_ami_id" {
  default = ""
}

variable "base_instance_type" {
  default = ""
}

variable "base_instance_name" {
  default = ""
}

variable "base_instance_tags" {
  default = {}
}


variable "base_subnet_id" {
  default = ""
}

variable "associate_public_ip_address" {
  default = true
}

variable "vpc_security_group_ids" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "base_ebs_device_name" {
  default = "/dev/xvda"
}

variable "base_ebs_volume_type" {
  default = "gp2"
}

variable "base_ebs_volume_size" {
  default = "30"
}

variable "base_ebs_delete_on_termination" {
  default = true
}

variable "base_ebs_encrypted" {
  default = false
}

variable "base_iam_instance_profile" {
  default = ""
}

variable "cpu_credits" {
  default = "standard"
}
