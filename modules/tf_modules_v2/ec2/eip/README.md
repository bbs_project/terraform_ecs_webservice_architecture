## ec2 eip module

### input variables
```
#ec2_instance_id
default : ""

#in_vpc
default : true
```

### output
```
#ec2_eip_id
#ec2_eip_public_ip
#ec2_eip_instance
```

### how to use
```
module "aws_eip" {
  source = "git::ssh://git@tool.devsep.com:7999/infra/tf_modules_v2//ec2/key_pair"
  ec2_instance_id = "${var.ec2_instance_id}"
  in_vpc = "${var.in_vpc}"
}
```

### references
https://www.terraform.io/docs/providers/aws/r/eip.html
