# ec2 eip module output

output "ec2_eip_id" {
  value = "${aws_eip.ec2.id}"
}

output "ec2_eip_public_ip" {
  value = "${aws_eip.ec2.public_ip}"
}

output "ec2_eip_instance" {
  value = "${aws_eip.ec2.instance}"
}
