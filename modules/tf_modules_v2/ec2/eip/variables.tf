# eip module variables

variable "ec2_instance_id" {
  default = ""
}

variable "in_vpc" {
  default = true
}

variable "ip_instance_name" {
  default = ""
}

variable "ip_instance_tags" {
  default = {}
}

