# elastic ip
resource "aws_eip" "ec2" {
  instance    = "${var.ec2_instance_id}"
  vpc         = "${var.in_vpc}"
  tags        = "${merge(map("Name", "${var.ip_instance_name}"), var.ip_instance_tags)}"
}
