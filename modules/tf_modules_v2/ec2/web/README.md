## ec2/web module variables :
web_ami_id
web_instance_type
web_instance_name
web_subnet_id
key_name
vpc_id
web_security_group_name
ssh_allowed_ips
ssh_allowed_sgs
mysql_allowed_sgs
http_allowed_ips
