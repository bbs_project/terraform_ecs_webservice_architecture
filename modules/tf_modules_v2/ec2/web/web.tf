resource "aws_instance" "web" {
  lifecycle {
    ignore_changes = ["ebs_block_device"]
  }
  ami                                 = "${var.web_ami_id}"
  instance_type                       = "${var.web_instance_type}"

  volume_tags                         = "${merge(map("Name", "${var.web_instance_name}"), var.web_instance_tags)}"
  tags                                = "${merge(map("Name", "${var.web_instance_name}"), var.web_instance_tags)}"

  subnet_id                           = "${element(split(",", var.web_subnet_id), count.index)}"
  associate_public_ip_address         = true
  vpc_security_group_ids              = ["${aws_security_group.web.id}"]
  key_name                            = "${var.key_name}"
  iam_instance_profile                = "${var.web_iam_instance_profile}"
  ebs_block_device = {
    device_name = "${var.web_ebs_device_name}"
    volume_type = "${var.web_ebs_volume_type}"
    volume_size = "${var.web_ebs_volume_size}"
    delete_on_termination = "${var.web_ebs_delete_on_termination}"
    encrypted = "${var.web_ebs_encrypted}"
  }
  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }
}

resource "aws_eip" "web" {
  instance                            = "${aws_instance.web.id}"
  vpc                                 = true
  tags                                = "${merge(map("Name", "${var.web_instance_name}"), var.ip_instance_tags)}"
}
