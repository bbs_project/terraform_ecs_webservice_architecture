resource "aws_security_group" "web" {
  lifecycle { create_before_destroy   = true }
  name                                = "${var.web_security_group_name}"
  description                         = "web security groups"
  vpc_id                              = "${var.vpc_id}"
  tags {
    Name                              = "${var.web_security_group_name}"
  }
  # HTTP/S access from IP, Security Group
  ingress {
    from_port                         = 80
    to_port                           = 80
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.http_allowed_ips)}"]
  }

  ingress {
    from_port                         = 443
    to_port                           = 443
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.http_allowed_ips)}"]
  }

  ingress {
    from_port                         = 22
    to_port                           = 22
    protocol                          = "tcp"
    cidr_blocks                       = ["${split(",", var.ssh_allowed_ips)}"]
  }

  ingress {
    from_port                         = 22
    to_port                           = 22
    protocol                          = "tcp"
    security_groups                   = ["${split(",", var.ssh_allowed_sgs)}"]
  }

  # allow mysql for security group
  ingress {
    from_port                         = 3306
    to_port                           = 3306
    protocol                          = "tcp"
    security_groups                   = ["${split(",", var.mysql_allowed_sgs)}"]
  }

  # outbound internet access
  egress {
    from_port                         = 0
    to_port                           = 0
    protocol                          = "-1"
    cidr_blocks                       = ["0.0.0.0/0"]
  }
}
