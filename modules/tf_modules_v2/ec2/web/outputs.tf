output "web_security_group_id" {
  value = "${aws_security_group.web.id}"
}

output "web_instance_id" {
  value = "${aws_instance.web.id}"
}
