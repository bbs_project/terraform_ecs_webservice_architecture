variable "web_ami_id" {
  default = ""
}

variable "web_instance_type" {
  default = ""
}

variable "web_instance_name" {
  default = ""
}

variable "web_instance_tags" {
  default = {}
}

variable "ip_instance_tags" {
  default = {}
}


variable "web_subnet_id" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "web_security_group_name" {
  default = ""
}

variable "ssh_allowed_ips" {
  default = ""
}

variable "ssh_allowed_sgs" {
  default = ""
}

variable "mysql_allowed_sgs" {
  default = ""
}

variable "http_allowed_ips" {
  default = ""
}

variable "web_ebs_device_name" {
  default = "/dev/xvda"
}

variable "web_ebs_volume_type" {
  default = "gp2"
}

variable "web_ebs_volume_size" {
  default = "30"
}

variable "web_ebs_delete_on_termination" {
  default = true
}

variable "web_ebs_encrypted" {
  default = false
}

variable "web_iam_instance_profile" {
  default = ""
}

variable "cpu_credits" {
  default = "standard"
}