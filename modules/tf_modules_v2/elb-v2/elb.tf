resource "aws_elb" "elb" {
  name = "${var.elb_name}"
  subnets = ["${split(",", var.elb_subnet_ids)}"]

  listener {
    instance_port       = "${var.elb_instance_port}"
    instance_protocol   = "${var.elb_instance_protocol}"
    lb_port             = "${var.elb_port}"
    lb_protocol         = "${var.elb_protocol}"
  }

  listener {
    instance_port       = "${var.elb_ssl_instance_port}"
    instance_protocol   = "${var.elb_ssl_instance_protocol}"
    lb_port             = "${var.elb_ssl_port}"
    lb_protocol         = "${var.elb_ssl_protocol}"
    ssl_certificate_id  = "${var.elb_ssl_certificate_id}"
  }

  health_check {
    healthy_threshold = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout = "${var.health_check_timeout}"
    target = "${var.health_check_target}"
    interval = "${var.health_check_interval}"
  }
  security_groups       = ["${split(",", var.aws_security_group_ids)}"]

  instances = ["${var.elb_instance_ids}"]
  cross_zone_load_balancing = "${var.elb_cross_zone_load_balancing}"
  idle_timeout = "${var.elb_idle_timeout}"
  connection_draining = "${var.elb_connection_draining}"
  connection_draining_timeout = "${var.elb_connection_draining_timeout}"

  tags {
      Name              = "${var.elb_name}"
  }
}
