# Setup VPC resources

- vpc
- network gateway

## Variables
#### Customize filed which use to config a new resource

- cidr_block: CDIR blocks
- vpc_name: The name of vpc which will be displayed on AWS console
- azs: Indicate the specific region where the new subnet available

## Outputs
#### Output parameters of the created resource which will be printed on the console

- vpc_id: ID of created VPC resource
- vpc_cidr: CDIR blocks
- gateway_id: ID of gateway that created along with the VPC
