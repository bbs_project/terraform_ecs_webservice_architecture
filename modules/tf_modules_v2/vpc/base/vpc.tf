#--------------------------------------------------------------
# This module creates all resources necessary for a VPC
#--------------------------------------------------------------

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  tags = merge(
    {
      "Name" = var.vpc_name
    },
    var.vpc_tags,
  )
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_internet_gateway" "public" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    {
      "Name" = var.vpc_name
    },
    var.vpc_tags,
  )
}

