output "public_subnet_ids" {
  value = join(",", aws_subnet.public.*.id)
}

