#--------------------------------------------------------------
# This module creates all resources necessary for a public
# subnet
#--------------------------------------------------------------

resource "aws_subnet" "public" {
  vpc_id            = var.vpc_id
  cidr_block        = element(split(",", var.public_subnet_cidrs), count.index)
  availability_zone = element(split(",", var.azs), count.index)
  count             = length(split(",", var.public_subnet_cidrs))
  tags = {
    Name = "${var.public_subnet_name}.${element(split(",", var.azs), count.index)}"
  }
  lifecycle {
    create_before_destroy = true
  }
  map_public_ip_on_launch = true
}

resource "aws_route_table" "public" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.gateway_id
  }
  tags = {
    Name = "${var.public_subnet_name}.${element(split(",", var.azs), count.index)}"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(split(",", var.public_subnet_cidrs))
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

