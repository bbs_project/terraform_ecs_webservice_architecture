variable "vpc_id" {
  default = ""
}

variable "nat_gateway_name" {
  default = "nat-gateway"
}

variable "nat_gateway_subnet_name" {
  default = "nat-gateway-subnet"
}

variable "nat_gateway_subnet_cidrs" {
  default = "10.1.20.0/24"
}

variable "internet_gateway_id" {
}

variable "azs" {
  default = "ap-northeast-1a,ap-northeast-1c"
}

