resource "aws_iam_server_certificate" "ssl_cert" {
  name_prefix      = "${var.ssl_cert_name}"
  /*certificate_body = "${var.certificate_body_file}"
  private_key      = "${var.private_key_file}"*/
  certificate_body = "${file("${path.cwd}/ssl/certificate.pem")}"
  private_key      = "${file("${path.cwd}/ssl/private_key.pem")}"

  lifecycle {
    create_before_destroy = true
  }
}
