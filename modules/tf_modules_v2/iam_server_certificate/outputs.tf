output "aws_iam_server_certificate_id" {
  value = "${aws_iam_server_certificate.ssl_cert.id}"
}

output "aws_iam_server_certificate_name" {
  value = "${aws_iam_server_certificate.ssl_cert.name}"
}

output "aws_iam_server_certificate_arn" {
  value = "${aws_iam_server_certificate.ssl_cert.arn}"
}
