provider "aws" {
    alias                = "ap-northeast-1"
    region               = "ap-northeast-1"
}

resource "aws_ecr_repository" "main" {
  provider               = "aws.ap-northeast-1"
  name                   = "${var.ecr_name}"
}

output "arn" {
  value                  = "${aws_ecr_repository.main.arn}"
}

output "registry_id" {
  value                  = "${aws_ecr_repository.main.registry_id}"
}

output "repository_url" {
  value                  = "${aws_ecr_repository.main.repository_url}"
}
