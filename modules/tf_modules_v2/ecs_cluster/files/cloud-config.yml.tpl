#!/bin/bash

echo 'ECS_CLUSTER=${name}' > /etc/ecs/ecs.config
echo 'ECS_ENGINE_AUTH_TYPE=${docker_auth_type}' >> /etc/ecs/ecs.config
echo 'ECS_ENGINE_AUTH_DATA=${docker_auth_data}' >> /etc/ecs/ecs.config
