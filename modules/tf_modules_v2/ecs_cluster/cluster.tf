resource "aws_security_group" "cluster" {
  name                                    = "${var.ecs_cluster_name}-ecs-cluster"
  vpc_id                                  = "${var.vpc_id}"
  description                             = "Allows traffic from and to the EC2 instances of the ${var.ecs_cluster_name} ECS cluster"

  ingress {
    from_port                             = 0
    to_port                               = 0
    protocol                              = -1
    security_groups                       = ["${split(",", var.ecs_cluster_security_groups)}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.http_allowed_ips)}"]
  }

  egress {
    from_port                             = 0
    to_port                               = 0
    protocol                              = -1
    cidr_blocks                           = ["0.0.0.0/0"]
  }

  tags {
    Name                                  = "ECS cluster (${var.ecs_cluster_name})"
    Environment                           = "${var.env}"
  }

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_ecs_cluster" "main" {
  name                                    = "${var.ecs_cluster_name}"

  lifecycle {
    create_before_destroy                 = true
  }
}

data "template_file" "cloud_config" {
  template                                = "${file("${path.module}/files/cloud-config.yml.tpl")}"

  vars {
    name                                  = "${var.ecs_cluster_name}"
    docker_auth_type                      = "${var.ecs_cluster_docker_auth_type}"
    docker_auth_data                      = "${var.ecs_cluster_docker_auth_data}"
  }

  /*lifecycle {
    create_before_destroy                 = true
  }*/
}

resource "aws_launch_configuration" "main" {
  name_prefix                             = "${format("%s-", var.ecs_cluster_name)}"

  image_id                                = "${var.ecs_cluster_image_id}"
  instance_type                           = "${var.ecs_cluster_instance_type}"
  ebs_optimized                           = "${var.ecs_cluster_instance_ebs_optimized}"
  iam_instance_profile                    = "${var.ecs_cluster_iam_instance_profile}"
  key_name                                = "${var.ecs_cluster_key_name}"
  security_groups                         = ["${aws_security_group.cluster.id}"]
  user_data                               = "${data.template_file.cloud_config.rendered}"
  associate_public_ip_address             = "${var.associate_public_ip_address}"

  # root
  root_block_device {
    volume_type                           = "gp2"
    volume_size                           = "${var.ecs_cluster_root_volume_size}"
  }

  # docker
  ebs_block_device {
    device_name                           = "/dev/xvdcz"
    volume_type                           = "gp2"
    volume_size                           = "${var.ecs_cluster_docker_volume_size}"
  }

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_autoscaling_group" "main" {
  name                                    = "${var.ecs_cluster_name}"

  availability_zones                      = ["${split(",", var.availability_zones)}"]
  vpc_zone_identifier                     = ["${split(",", var.ecs_cluster_subnet_ids)}"]
  launch_configuration                    = "${aws_launch_configuration.main.id}"
  min_size                                = "${var.ecs_cluster_min_size}"
  max_size                                = "${var.ecs_cluster_max_size}"
  desired_capacity                        = "${var.ecs_cluster_desired_capacity}"
  termination_policies                    = ["OldestLaunchConfiguration", "Default"]

  tag {
    key                                   = "Name"
    value                                 = "${var.ecs_cluster_name}"
    propagate_at_launch                   = true
  }

  tag {
    key                                   = "Cluster"
    value                                 = "${var.ecs_cluster_name}"
    propagate_at_launch                   = true
  }

  tag {
    key                                   = "Environment"
    value                                 = "${var.env}"
    propagate_at_launch                   = true
  }

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_autoscaling_policy" "scale_up" {
  name                                    = "${var.ecs_cluster_name}-scaleup"
  scaling_adjustment                      = 1
  adjustment_type                         = "ChangeInCapacity"
  cooldown                                = 300
  autoscaling_group_name                  = "${aws_autoscaling_group.main.name}"

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_autoscaling_policy" "scale_down" {
  name                                    = "${var.ecs_cluster_name}-scaledown"
  scaling_adjustment                      = -1
  adjustment_type                         = "ChangeInCapacity"
  cooldown                                = 300
  autoscaling_group_name                  = "${aws_autoscaling_group.main.name}"

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name                              = "${var.ecs_cluster_name}-cpureservation-high"
  comparison_operator                     = "GreaterThanOrEqualToThreshold"
  evaluation_periods                      = "2"
  metric_name                             = "CPUReservation"
  namespace                               = "AWS/ECS"
  period                                  = "300"
  statistic                               = "Maximum"
  threshold                               = "90"

  dimensions {
    ClusterName                           = "${aws_ecs_cluster.main.name}"
  }

  alarm_description                       = "Scale up if the cpu reservation is above 90% for 10 minutes"
  alarm_actions                           = ["${aws_autoscaling_policy.scale_up.arn}"]

  lifecycle {
    create_before_destroy                 = true
  }
}

resource "aws_cloudwatch_metric_alarm" "memory_high" {
  alarm_name                              = "${var.ecs_cluster_name}-memoryreservation-high"
  comparison_operator                     = "GreaterThanOrEqualToThreshold"
  evaluation_periods                      = "2"
  metric_name                             = "MemoryReservation"
  namespace                               = "AWS/ECS"
  period                                  = "300"
  statistic                               = "Maximum"
  threshold                               = "90"

  dimensions {
    ClusterName                           = "${aws_ecs_cluster.main.name}"
  }

  alarm_description                       = "Scale up if the memory reservation is above 90% for 10 minutes"
  alarm_actions                           = ["${aws_autoscaling_policy.scale_up.arn}"]

  lifecycle {
    create_before_destroy                 = true
  }

  # This is required to make cloudwatch alarms creation sequential, AWS doesn't
  # support modifying alarms concurrently.
  depends_on                              = ["aws_cloudwatch_metric_alarm.cpu_high"]
}

resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name                              = "${var.ecs_cluster_name}-cpureservation-low"
  comparison_operator                     = "LessThanOrEqualToThreshold"
  evaluation_periods                      = "2"
  metric_name                             = "CPUReservation"
  namespace                               = "AWS/ECS"
  period                                  = "300"
  statistic                               = "Maximum"
  threshold                               = "10"

  dimensions {
    ClusterName                           = "${aws_ecs_cluster.main.name}"
  }

  alarm_description                       = "Scale down if the cpu reservation is below 10% for 10 minutes"
  alarm_actions                           = ["${aws_autoscaling_policy.scale_down.arn}"]

  lifecycle {
    create_before_destroy                 = true
  }

  # This is required to make cloudwatch alarms creation sequential, AWS doesn't
  # support modifying alarms concurrently.
  depends_on                              = ["aws_cloudwatch_metric_alarm.memory_high"]
}

resource "aws_cloudwatch_metric_alarm" "memory_low" {
  alarm_name                              = "${var.ecs_cluster_name}-memoryreservation-low"
  comparison_operator                     = "LessThanOrEqualToThreshold"
  evaluation_periods                      = "2"
  metric_name                             = "MemoryReservation"
  namespace                               = "AWS/ECS"
  period                                  = "300"
  statistic                               = "Maximum"
  threshold                               = "10"

  dimensions {
    ClusterName                           = "${aws_ecs_cluster.main.name}"
  }

  alarm_description                       = "Scale down if the memory reservation is below 10% for 10 minutes"
  alarm_actions                           = ["${aws_autoscaling_policy.scale_down.arn}"]

  lifecycle {
    create_before_destroy                 = true
  }

  # This is required to make cloudwatch alarms creation sequential, AWS doesn't
  # support modifying alarms concurrently.
  depends_on                              = ["aws_cloudwatch_metric_alarm.cpu_low"]
}

// The cluster name, e.g cdn
output "name" {
  value                                   = "${aws_ecs_cluster.main.name}"
}

// The cluster security group ID.
output "security_group_id" {
  value                                   = "${aws_security_group.cluster.id}"
}
