variable "ecs_cluster_name"                         {description = "The cluster name, e.g cdn"}
variable "env"                                      {description = "Environment tag, e.g prod"}
variable "vpc_id"                                   {description = "VPC ID"}
variable "http_allowed_ips"                         {}
variable "ecs_cluster_image_id"                     {description = "AMI Image ID"}
variable "ecs_cluster_subnet_ids"                   {description = "Comma separated list of subnet IDs"}
variable "ecs_cluster_key_name"                     {description = "SSH key name to use"}
variable "ecs_cluster_security_groups"              {description = "Comma separated list of security groups"}
variable "ecs_cluster_iam_instance_profile"         {description = "Instance profile ARN to use in the launch configuration"}
variable "region"                                   {description = "AWS Region"}
variable "availability_zones"                       {description = "Comma separated list of AZs"}
variable "ecs_cluster_instance_type"                {description = "The instance type to use, e.g t2.small"}
variable "ecs_cluster_instance_ebs_optimized"       {description = "When set to true the instance will be launched with EBS optimized turned on"
  default     = true
}
variable "ecs_cluster_min_size"                     {description = "Minimum instance count"
  default     = 3
}
variable "ecs_cluster_max_size"                     {description = "Maxmimum instance count"
  default     = 100
}
variable "ecs_cluster_desired_capacity"             {description = "Desired instance count"
  default     = 3
}
variable "associate_public_ip_address"              {description = "Should created instances be publicly accessible (if the SG allows)"
  default = false
}
variable "ecs_cluster_root_volume_size"             {description = "Root volume size in GB"
  default     = 25
}
variable "ecs_cluster_docker_volume_size"           {description = "Attached EBS volume size in GB"
  default     = 25
}
variable "ecs_cluster_docker_auth_type"             {description = "The docker auth type, see https://godoc.org/github.com/aws/amazon-ecs-agent/agent/engine/dockerauth for the possible values"
  default     = ""
}
variable "ecs_cluster_docker_auth_data"             {description = "A JSON object providing the docker auth data, see https://godoc.org/github.com/aws/amazon-ecs-agent/agent/engine/dockerauth for the supported formats"
  default     = ""
}
