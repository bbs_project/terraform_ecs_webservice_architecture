# AWS-Batch-Terraform

```
module "compute-environment" {
  source = "<<PATH_TO_MODULE>>"

  compute_environment_name = "${var.env}-${var.compute_environment_name}"
  instance_role            = "${module.ecs_instance_role.iprofile_arn}"
  service_role             = "${module.aws_batch_service_role.iam_role_arn}"
  instance_type            = "${var.instance_type}"
  max_vcpus                = "${var.max_vcpus}"
  min_vcpus                = "${var.min_vcpus}"
  ec2_key_pair             = "${data.terraform_remote_state.3rd_party_web_key_pair.key_name}"
  subnets_ids              = "${data.terraform_remote_state.dev_private_subnet_ids.private_subnet_ids}"

  vpc_id                   = "${data.terraform_remote_state.3rd_party_vpc.vpc_id}"
  ssh_allowed_ips          = "${var.ssh_allowed_ips}"
}
```