 output "base_arn" {
   value = "${aws_batch_compute_environment.base.arn}"
}

output "base_status" {
   value = "${aws_batch_compute_environment.base.status}"
}

output "base_status_reason" {
   value = "${aws_batch_compute_environment.base.status_reason}"
}

output "base_ecs_cluster_arn" {
   value = "${aws_batch_compute_environment.base.ecs_cluster_arn}"
}