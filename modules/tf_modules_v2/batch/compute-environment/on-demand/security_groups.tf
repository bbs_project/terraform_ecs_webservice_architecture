resource "aws_security_group" "compute_environment" {
  name                  = "${var.compute_environment_name}-security-group"
  description           = "compute environment security group"
  vpc_id                = "${var.vpc_id}"
  tags                  = "${merge(map("Name", "${var.compute_environment_name}-security-group"), var.compute_environment_tags)}"

  ingress {
    from_port           = 22
    to_port             = 22
    protocol            = "tcp"
    cidr_blocks         = ["${split(",", var.ssh_allowed_ips)}"]
  }

  # outbound internet access
  egress {
    from_port           = 0
    to_port             = 0
    protocol            = "-1"
    cidr_blocks         = ["0.0.0.0/0"]
  }
}
