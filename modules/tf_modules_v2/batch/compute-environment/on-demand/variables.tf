variable "instance_role" {
  default = ""
}

variable "ec2_key_pair" {
  default = ""
}

variable "compute_environment_name" {
  default = ""
}

variable "state" {
  default = "ENABLED"
}


variable "instance_type" {
  default = "c5,m5"
}

variable "max_vcpus" {
  default = "4"
}

variable "desired_vcpus" {
  default = "0"
}

variable "min_vcpus" {
  default = "0"
}

variable "vpc_security_group_ids" {
  default = ""
}

variable "subnets_ids" {
  default = ""
}

variable  "service_role" {
  default = ""
}

variable "type_batch" {
  default = "MANAGED"
}

variable "image_id" {
  default = ""
}

variable "other_name_tags" {
  default = {}
}

variable "type_ec2" {
  default = "EC2"
}

## Security Group

variable "compute_environment_tags" {
  default = {}
}

variable "vpc_id" {
  default = ""
}

variable "ssh_allowed_ips" {
  default = ""
}

