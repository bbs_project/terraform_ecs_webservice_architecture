locals {
  compute_environment_sg = "${aws_security_group.compute_environment.id},${var.vpc_security_group_ids}"
}


locals {
  list_vpc_security_group_ids = "${var.vpc_security_group_ids != "" ? local.compute_environment_sg : aws_security_group.compute_environment.id}"
}

resource "aws_batch_compute_environment" "base" {
  compute_environment_name = "${var.compute_environment_name}"
  state                    = "${var.state}"

  compute_resources {
    instance_role            = "${var.instance_role}"
    instance_type            = ["${split(",",var.instance_type)}"]

    image_id                 = "${var.image_id}"
    max_vcpus                = "${var.max_vcpus}"
    desired_vcpus            = "${var.desired_vcpus}"
    min_vcpus                = "${var.min_vcpus}"
    ec2_key_pair             = "${var.ec2_key_pair}"

    security_group_ids       = ["${split(",", local.list_vpc_security_group_ids)}"]

    subnets                  = ["${split(",", var.subnets_ids)}"]
    
    type                     = "${var.type_ec2}"
    tags                     = "${merge(map("Name", "${var.compute_environment_name}"), var.other_name_tags)}"

  }
    service_role             = "${var.service_role}"
    type                     = "${var.type_batch}"
}