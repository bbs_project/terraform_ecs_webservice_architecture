## common variables
variable "env" {
}

variable "vpc_id" {
}

# aurora cluster variables
variable "rds_aurora_cluster_identifier" {
}

variable "azs" {
  description = "A comma-separated list of availability zones"
}

variable "rds_aurora_database_name" {
  description = "The database name"
}

variable "rds_aurora_master_username" {
  description = "The master user username"
}

variable "rds_aurora_master_password" {
  description = "The master user password"
}

variable "rds_aurora_backup_retention_period" {
}

variable "rds_aurora_preferred_backup_window" {
}

variable "rds_aurora_port" {
}

/*variable "rds_aurora_engine" {
  default = ""
}
variable "rds_aurora_engine_version" {
  default = ""
}*/
variable "skip_final_snapshot" {
  default = ""
}

## aurora instance variables
variable "rds_aurora_cluster_instance_identifier" {
  default = ""
}

variable "rds_aurora_publicly_accessible" {
  default = ""
}

variable "rds_aurora_instance_type" {
  default = ""
}

variable "rds_aurora_instance_count" {
}

## security_groups variables
variable "rds_aurora_allowed_sgs" {
  description = "A comma-separated list of security group IDs"
}

## subnets variables
variable "rds_aurora_private_subnet_ids" {
}

## route53 variables
/*variable "rds_aurora_dns_name"                {}
variable "route53_zone_id"                 {}*/
