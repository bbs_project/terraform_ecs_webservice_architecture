resource "aws_security_group" "rds_aurora" {
  name        = "${var.rds_aurora_cluster_identifier}-rds-cluster"
  description = "Allows traffic to rds from other security groups"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = var.rds_aurora_port
    to_port         = var.rds_aurora_port
    protocol        = "TCP"
    security_groups = split(",", var.rds_aurora_allowed_sgs)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "RDS cluster (${var.rds_aurora_cluster_identifier})"
    Environment = var.env
  }
}

