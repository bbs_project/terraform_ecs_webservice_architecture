#--------------------------------------------------------------
# This module creates all resources necessary for a rds_aurora
# subnet
#--------------------------------------------------------------

resource "aws_db_subnet_group" "rds_aurora" {
  name        = "${var.env}_main_subnet_group"
  description = "${var.env} rds subnet group"
  subnet_ids  = [element(split(",", var.rds_aurora_private_subnet_ids), 1), element(split(",", var.rds_aurora_private_subnet_ids), 2)]
}

