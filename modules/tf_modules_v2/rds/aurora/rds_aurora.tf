resource "aws_rds_cluster_instance" "cluster_instances" {
  count                = var.rds_aurora_instance_count
  identifier           = var.rds_aurora_cluster_instance_identifier
  db_subnet_group_name = aws_db_subnet_group.rds_aurora.id
  cluster_identifier   = aws_rds_cluster.main.id
  publicly_accessible  = var.rds_aurora_publicly_accessible
  instance_class       = var.rds_aurora_instance_type
}

resource "aws_rds_cluster" "main" {
  cluster_identifier      = var.rds_aurora_cluster_identifier
  availability_zones      = split(",", var.azs)
  database_name           = var.rds_aurora_database_name
  master_username         = var.rds_aurora_master_username
  master_password         = var.rds_aurora_master_password
  backup_retention_period = var.rds_aurora_backup_retention_period
  preferred_backup_window = var.rds_aurora_preferred_backup_window
  vpc_security_group_ids  = [aws_security_group.rds_aurora.id]
  db_subnet_group_name    = aws_db_subnet_group.rds_aurora.id
  port                    = var.rds_aurora_port

  #engine_version = "${var.rds_aurora_engine_version}"
  skip_final_snapshot = var.skip_final_snapshot
}

/*resource "aws_route53_record" "main" {
  zone_id                             = "${var.route53_zone_id}"
  name                                = "${coalesce(var.rds_aurora_dns_name, var.rds_aurora_cluster_identifier)}"
  type                                = "CNAME"
  ttl                                 = 300
  records                             = ["${aws_rds_cluster.main.endpoint}"]
}*/
