## rds/aurora module variables
rds_aurora_name
env
vpc_id
rds_aurora_zone_id
allowed_security_groups
rds_aurora_subnet_ids
azs
rds_aurora_database_name
rds_aurora_master_username
rds_aurora_master_password
rds_aurora_instance_type
rds_aurora_instance_count
rds_aurora_preferred_backup_window
rds_aurora_backup_retention_period
rds_aurora_publicly_accessible
rds_aurora_dns_name
rds_aurora_port
