#--------------------------------------------------------------
# This module creates all resources necessary for a rds_postgresql
# subnet
#--------------------------------------------------------------

resource "aws_db_subnet_group" "rds_postgresql" {
  name        = "${var.env}_${var.rds_postgresql_identifier}_subnet_group"
  description = "${var.env} rds subnet group"
  subnet_ids  = [element(split(",", var.rds_postgresql_subnet_ids), 1), element(split(",", var.rds_postgresql_subnet_ids), 2)]
}

