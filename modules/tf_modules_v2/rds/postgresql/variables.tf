# environment variables
variable "env" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

# rds variables
variable "rds_postgresql_identifier" {
  default = "septech-rds-01"
}

variable "rds_postgresql_storage" {
  default = "10"
}

variable "rds_postgresql_engine" {
  default = "postgresql"
}

variable "rds_postgresql_engine_version" {
  default = ""
}

variable "rds_postgresql_instance_class" {
  default = ""
}

variable "rds_postgresql_storage_type" {
  default = "standard"
}

variable "rds_postgresql_skip_final_snapshot" {
  default = "false"
}

variable "rds_postgresql_db_name" {
  default = ""
}

variable "rds_postgresql_username" {
  default = ""
}

variable "rds_postgresql_password" {
  description = "password, provide through your ENV variables"
}

variable "rds_postgresql_availability_zone" {
  default = ""
}

variable "rds_postgresql_backup_retention_period" {
  default = "7"
}

#variable "aws_region"                       {}

variable "rds_postgresql_multi_az" {
  default = false
}

variable "rds_postgresql_port" {
  default = "5432"
}

variable "rds_postgresql_publicly_accessible" {
  default = false
}

# security_group variables
variable "rds_postgresql_allowed_ips" {
  default = ""
}

variable "rds_postgresql_sg_name" {
}

variable "rds_postgresql_apply_immediately" {
  default = false
}

variable "rds_postgresql_parameter_group_name" {
  default = ""
}


# subnet variables
variable "rds_postgresql_subnet_name" {
  default = "rds_postgresql"
}

variable "rds_postgresql_subnet_ids" {
  default = ""
}

variable "rds_postgresql_tags" {
  type = map(string)
  default = {
    mackerel = "No"
  }
}

