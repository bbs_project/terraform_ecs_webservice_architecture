# Create RDs using Postgresql 9.6

  - secirity groups
  - subnet group
  - db instance

## Variables

### env

### vpc_id

### rds_postgresql_identifier

### rds_postgresql_storage

### rds_postgresql_engine

### rds_postgresql_engine_version

### rds_postgresql_instance_class

### rds_postgresql_storage_type

### rds_postgresql_skip_final_snapshot

### rds_postgresql_db_name

### rds_postgresql_username

### rds_postgresql_password

### rds_postgresql_availability_zone

### rds_postgresql_backup_retention_period

### rds_postgresql_multi_az

### rds_postgresql_port

### rds_postgresql_publicly_accessible

### rds_postgresql_allowed_ips

### rds_postgresql_allowed_sgs

### rds_postgresql_sg_name

### rds_postgresql_apply_immediately

### rds_postgresql_subnet_name

### rds_postgresql_private_subnet_ids

###"rds_postgresql_tags

## Outputs
