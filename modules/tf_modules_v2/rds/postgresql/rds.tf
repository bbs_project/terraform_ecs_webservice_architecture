resource "aws_db_instance" "rds_postgresql" {
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_security_group.rds_postgresql,aws_db_subnet_group.rds_postgresql]

  allocated_storage       = var.rds_postgresql_storage
  engine                  = var.rds_postgresql_engine
  engine_version          = var.rds_postgresql_engine_version
  identifier              = var.rds_postgresql_identifier
  instance_class          = var.rds_postgresql_instance_class
  storage_type            = var.rds_postgresql_storage_type
  skip_final_snapshot     = var.rds_postgresql_skip_final_snapshot
  name                    = var.rds_postgresql_db_name
  password                = var.rds_postgresql_password
  username                = var.rds_postgresql_username
  availability_zone       = var.rds_postgresql_availability_zone
  backup_retention_period = var.rds_postgresql_backup_retention_period
  multi_az                = var.rds_postgresql_multi_az
  port                    = var.rds_postgresql_port
  publicly_accessible     = var.rds_postgresql_publicly_accessible
  vpc_security_group_ids  = [aws_security_group.rds_postgresql.id]
  db_subnet_group_name    = aws_db_subnet_group.rds_postgresql.id
  apply_immediately       = var.rds_postgresql_apply_immediately
  parameter_group_name    = var.rds_postgresql_parameter_group_name
  tags                    = var.rds_postgresql_tags
}

