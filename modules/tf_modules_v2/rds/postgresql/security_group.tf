resource "aws_security_group" "rds_postgresql" {
  lifecycle {
    create_before_destroy = true
  }
  name        = "${var.env}_${var.rds_postgresql_sg_name}"
  description = "rds postgresql security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "TCP"
    cidr_blocks = split(",", var.rds_postgresql_allowed_ips)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.rds_postgresql_sg_name
  }
}

