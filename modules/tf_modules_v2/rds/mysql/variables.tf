# environment variables
variable "env" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

# rds variables
variable "rds_mysql_identifier" {
  default = "septech-rds-01"
}

variable "rds_mysql_storage" {
  default = "10"
}

variable "rds_mysql_engine" {
  default = "mysql"
}

variable "rds_mysql_engine_version" {
  default = ""
}

variable "rds_mysql_instance_class" {
  default = ""
}

variable "rds_mysql_storage_type" {
  default = "standard"
}

variable "rds_mysql_skip_final_snapshot" {
  default = "false"
}

variable "rds_mysql_db_name" {
  default = ""
}

variable "rds_mysql_username" {
  default = ""
}

variable "rds_mysql_password" {
  description = "password, provide through your ENV variables"
}

variable "rds_mysql_availability_zone" {
  default = ""
}

variable "rds_mysql_backup_retention_period" {
  default = "7"
}

#variable "aws_region"                       {}

variable "rds_mysql_multi_az" {
  default = false
}

variable "rds_mysql_port" {
  default = "3306"
}

variable "rds_mysql_publicly_accessible" {
  default = false
}

# security_group variables
variable "rds_mysql_allowed_ips" {
  default = ""
}

variable "rds_mysql_allowed_sgs" {
  default = ""
}

variable "rds_mysql_sg_name" {
}

variable "rds_mysql_apply_immediately" {
  default = false
}

# subnet variables
variable "rds_mysql_subnet_name" {
  default = "rds_mysql"
}

variable "rds_mysql_subnet_ids" {
}

variable "rds_mysql_tags" {
  type = map(string)
  default = {
    mackerel = "No"
  }
}

