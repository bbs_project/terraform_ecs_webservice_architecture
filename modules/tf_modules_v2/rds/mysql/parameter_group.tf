resource "aws_db_parameter_group" "rds_mysql" {
  /*lifecycle {
      ignore_changes = ["parameter"]
    }*/
  name   = var.parameter_group_name
  family = var.rds_mysql_family

  parameter {
    name  = "character_set_server"
    value = var.parameter_character_set_server
  }

  parameter {
    name  = "collation_server"
    value = var.parameter_collation_server
  }

  parameter {
    name  = "character_set_client"
    value = var.parameter_character_set_client
  }

  parameter {
    name         = "character-set-client-handshake"
    value        = var.parameter_character-set-client-handshake
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "key_buffer_size"
    value        = var.parameter_key_buffer_size
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "table_open_cache"
    value        = var.parameter_table_open_cache
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "innodb_sort_buffer_size"
    value        = var.parameter_innodb_sort_buffer_size
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "read_buffer_size"
    value        = var.parameter_read_buffer_size
    apply_method = "pending-reboot"
  }

  parameter {
    name  = "slow_query_log"
    value = var.parameter_slow_query_log
    #apply_method = "pending-reboot"
  }

  parameter {
    name  = "long_query_time"
    value = var.parameter_long_query_time
    #apply_method = "pending-reboot"
  }

  parameter {
    name  = "time_zone"
    value = var.parameter_time_zone
    #apply_method = "pending-reboot"
  }
}

variable "parameter_group_name" {
  default = "rds-mysql-parameter-group-5-6"
}

variable "rds_mysql_family" {
  default = "mysql5.6"
}

variable "parameter_character_set_server" {
  default = "utf8"
}

variable "parameter_collation_server" {
  default = "utf8_general_ci"
}

variable "parameter_character_set_client" {
  default = "utf8"
}

variable "parameter_character-set-client-handshake" {
  default = true
}

variable "parameter_key_buffer_size" {
  default = "16777216"
}

variable "parameter_table_open_cache" {
  default = "2000"
}

variable "parameter_innodb_sort_buffer_size" {
  default = "1048576"
}

variable "parameter_read_buffer_size" {
  default = "262144"
}

variable "parameter_slow_query_log" {
  default = "0"
}

variable "parameter_long_query_time" {
  default = "0"
}

variable "parameter_time_zone" {
  default = "UTC"
}

