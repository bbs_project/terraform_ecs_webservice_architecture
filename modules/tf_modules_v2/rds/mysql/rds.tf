resource "aws_db_instance" "rds_mysql" {
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_security_group.rds_mysql]
  depends_on = [aws_db_subnet_group.rds_mysql]

  allocated_storage = var.rds_mysql_storage
  engine            = var.rds_mysql_engine
  engine_version    = var.rds_mysql_engine_version
  identifier        = var.rds_mysql_identifier
  instance_class    = var.rds_mysql_instance_class
  storage_type      = var.rds_mysql_storage_type

  #final_snapshot_identifier
  skip_final_snapshot = var.rds_mysql_skip_final_snapshot

  #copy_tags_to_snapshot
  name                    = var.rds_mysql_db_name
  password                = var.rds_mysql_password
  username                = var.rds_mysql_username
  availability_zone       = var.rds_mysql_availability_zone
  backup_retention_period = var.rds_mysql_backup_retention_period

  #backup_window
  #iops
  #maintenance_window
  multi_az               = var.rds_mysql_multi_az
  port                   = var.rds_mysql_port
  publicly_accessible    = var.rds_mysql_publicly_accessible
  vpc_security_group_ids = [aws_security_group.rds_mysql.id]

  #security_group_names
  db_subnet_group_name = aws_db_subnet_group.rds_mysql.id
  parameter_group_name = aws_db_parameter_group.rds_mysql.id

  #option_group_name
  #storage_encrypted
  apply_immediately = var.rds_mysql_apply_immediately

  #replicate_source_db
  #snapshot_identifier
  #license_model
  #auto_minor_version_upgrade
  #allow_major_version_upgrade
  #monitoring_role_arn
  #monitoring_interval
  #kms_key_id
  #character_set_name
  tags = merge(
    {
      "Name" = var.rds_mysql_identifier
    },
    var.rds_mysql_tags,
  )
}

