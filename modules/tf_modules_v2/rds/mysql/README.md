# Setup RDS/mysql resource

- rds_mysql

## Variables
#### Customize filed which use to config a new resource

- vpc_id: ID of VPC which the new rds_mysql belongs to.
- rds_mysql_identifier: Customize name of the rds resource
- rds_mysql_storage: The maximum storage of the new rds resource
- rds_mysql_engine: The database engine to use
- rds_mysql_engine_version: The engine version to use
- rds_mysql_instance_class: The instance type of the RDS instance
- rds_mysql_storage_type: One of "standard", "gp2", or "io1"
- rds_mysql_skip_final_snapshot: Determines whether a final DB snapshot is created before the DB instance is deleted
- rds_mysql_db_name: rds_mysql DB name
- rds_mysql_username: rds_mysql DB username
- rds_mysql_password: rds_mysql DB password
- rds_mysql_availability_zone: The availability zone (AZ) for the RDS instance
- rds_mysql_backup_retention_period: The days to retain backups for
- rds_mysql_multi_az: Specifies if the RDS instance is multi-AZ
- rds_mysql_port: The port on which the DB accepts connections
- rds_mysql_publicly_accessible: Bool to control if instance is publicly accessible
- rds_mysql_allowed_ips: The CIDR block to accept
- rds_mysql_allowed_sgs: The name of the security group to authorize
- rds_mysql_sg_name: List of DB Security Groups to associate
- rds_mysql_subnet_name: Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group
- rds_mysql_subnet_ids: A list of VPC subnet IDs
- rds_mysql_tags: A mapping of tags to assign to all resources

## Outputs
#### Output parameters of the created resource which will be printed on the console
- id: ID of created rds_mysql
