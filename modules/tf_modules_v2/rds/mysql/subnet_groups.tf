#--------------------------------------------------------------
# This module creates all resources necessary for a rds_mysql
# subnet
#--------------------------------------------------------------

resource "aws_db_subnet_group" "rds_mysql" {
  name        = "${var.env}_main_subnet_group"
  description = "${var.env} rds subnet group"

  # 2 subnets are required at least
  subnet_ids = [element(split(",", var.rds_mysql_subnet_ids), 1), element(split(",", var.rds_mysql_subnet_ids), 2)]
}

