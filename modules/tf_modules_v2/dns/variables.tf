variable "route53_zone_name" {
  description = "Zone name, e.g septech.dev"
}

variable "vpc_id" {
  description = "The VPC ID (omit to create a public zone)"
  default     = ""
}
