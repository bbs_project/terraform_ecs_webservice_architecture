variable "env"                                {description = "Environment tag, e.g prod"}
# ecs task variables
variable "ecs_task_image"                     {description = "The docker image name, e.g nginx"}
variable "ecs_task_name"                      {description = "The service name, if empty the service name is defaulted to the image name"
                                              default     = ""}
variable "ecs_task_image_version"             {description = "The docker image version"
                                              default     = "latest"}
variable "ecs_task_ports"                     {description = "The container host port"}
variable "ecs_task_command"                   {description = "The raw json of the task command"
                                              default     = "[]"}
variable "ecs_task_cpu"                       {description = "The number of cpu units to reserve for the container"
                                              default     = 512}
variable "ecs_task_memory"                    {description = "The number of MiB of memory to reserve for the container"
                                              default     = 512}
variable "ecs_task_env_vars"                  {description = "The raw json of the task env vars"
                                              default     = "[]"}
# elb variables
variable "ecs_elb_subnet_ids"                 {description = "Comma separated list of subnet IDs that will be passed to the ELB module"}
variable "ecs_elb_security_group_name"        {description = "Comma separated list of security group IDs that will be passed to the ELB module"}
#variable "ecs_service_log_bucket"             {description = "The S3 bucket ID to use for the ELB"}
#variable "ecs_elb_health_check"               {description = "Path to a healthcheck endpoint"
#                                              default     = "/"}
variable "ecs_elb_protocol"                   {description = "The ELB protocol, HTTP or TCP"
                                              default     = "HTTP"}
variable "ecs_elb_instance_protocol"          {description = "The ELB protocol, HTTP or TCP"
                                              default     = "HTTP"}
variable "ecs_elb_port"                       {}
variable "ecs_elb_instance_port"              {}
variable "ssh_allowed_ips"                    {}
variable "http_allowed_ips"                   {}
variable "vpc_id"                             {}
variable "azs"                                {description = "The zone ID to create the record in"}

# ecs service variables
variable "ecs_service_cluster"                {description = "The cluster name or ARN"}
variable "ecs_service_container_port"         {description = "The container port"
                                              default     = 3000}
variable "ecs_service_desired_count"          {description = "The desired count"
                                              default     = 2}
variable "ecs_service_iam_role"               {description = "IAM Role ARN to use"}
#variable "ecs_service_dns_name"              {description = "The DNS name to use, e.g nginx"}
