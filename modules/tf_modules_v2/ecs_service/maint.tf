resource "aws_ecs_service" "main" {
  name            = "${module.task.name}"
  cluster         = "${var.ecs_service_cluster}"
  task_definition = "${module.task.arn}"
  desired_count   = "${var.ecs_service_desired_count}"
  iam_role        = "${var.ecs_service_iam_role}"

  load_balancer {
    elb_name       = "${module.elb.id}"
    container_name = "${module.task.name}"
    container_port = "${var.ecs_service_container_port}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

module "task" {
  source = "../ecs_task"

  ecs_task_name          = "${var.ecs_task_name}"
  ecs_task_image         = "${var.ecs_task_image}"
  ecs_task_image_version = "${var.ecs_task_image_version}"
  ecs_task_command       = "${var.ecs_task_command}"
  ecs_task_env_vars      = "${var.ecs_task_env_vars}"
  ecs_task_memory        = "${var.ecs_task_memory}"
  ecs_task_cpu           = "${var.ecs_task_cpu}"
  ecs_task_ports         = "${var.ecs_task_ports}"
}

module "elb" {
  source = "../elb"

  elb_name                = "${module.task.name}"
  elb_port                = "${var.ecs_elb_port}"
  elb_instance_port       = "${var.ecs_elb_instance_port}"
  #environment            = "${var.environment}"
  elb_subnet_ids          = "${var.ecs_elb_subnet_ids}"
  elb_security_group_name = "${var.ecs_elb_security_group_name}"
  #dns_name               = "${coalesce(var.dns_name, module.task.name)}"
  /*elb_health_check        = "${var.ecs_elb_health_check}"*/
  elb_instance_protocol   = "${var.ecs_elb_instance_protocol}"
  elb_protocol            = "${var.ecs_elb_protocol}"
  vpc_id                  = "${var.vpc_id}"
  azs                     = "${var.azs}"
  http_allowed_ips        = "${var.http_allowed_ips}"
  ssh_allowed_ips         = "${var.ssh_allowed_ips}"
  #log_bucket              = "${var.log_bucket}"
}

/**
 * Outputs.
 */

// The name of the ELB
output "name" {
  value = "${module.elb.name}"
}

// The DNS name of the ELB
output "dns" {
  value = "${module.elb.dns}"
}

// The id of the ELB
output "elb" {
  value = "${module.elb.id}"
}

// The zone id of the ELB
output "zone_id" {
  value = "${module.elb.zone_id}"
}

// FQDN built using the zone domain and name
output "fqdn" {
  value = "${module.elb.fqdn}"
}
