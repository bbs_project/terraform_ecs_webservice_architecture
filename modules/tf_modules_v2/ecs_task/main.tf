/**
 * Resources.
 */

# The ECS task definition.

resource "aws_ecs_task_definition" "main" {
  family                  = "${var.ecs_task_name}"

  lifecycle {
    ignore_changes        = ["ecs_task_image"]
    create_before_destroy = true
  }

  container_definitions   = <<EOF
[
  {
    "cpu":                ${var.ecs_task_cpu},
    "environment":        ${var.ecs_task_env_vars},
    "essential":          true,
    "command":            ${var.ecs_task_command},
    "image":              "${var.ecs_task_image}:${var.ecs_task_image_version}",
    "memory":             ${var.ecs_task_memory},
    "name":               "${var.ecs_task_name}",
    "portMappings":       ${var.ecs_task_ports},
    "entryPoint":         ${var.ecs_task_entry_point},
    "mountPoints":        [],
    "logConfiguration":   {
      "logDriver":        "syslog",
      "options":          {
        "tag":            "${var.ecs_task_name}"
      }
    }
  }
]
EOF
}


// The created task definition name
output "name" {
  value                   = "${aws_ecs_task_definition.main.family}"
}

// The created task definition ARN
output "arn" {
  value                   = "${aws_ecs_task_definition.main.arn}"
}
