locals {
  rds_mysql_sg = "${aws_security_group.rds_mysql.id},${var.vpc_security_group_ids}"
}

locals {
  list_vpc_security_group_ids = var.vpc_security_group_ids != "" ? local.rds_mysql_sg : aws_security_group.rds_mysql.id
}

resource "aws_db_instance" "rds_mysql" {
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_security_group.rds_mysql, aws_db_subnet_group.rds_mysql]

  allocated_storage       = var.rds_mysql_storage
  engine                  = var.rds_mysql_engine
  engine_version          = var.rds_mysql_engine_version
  identifier              = var.rds_mysql_identifier
  instance_class          = var.rds_mysql_instance_class
  storage_type            = var.rds_mysql_storage_type
  skip_final_snapshot     = var.rds_mysql_skip_final_snapshot
  copy_tags_to_snapshot   = var.rds_mysql_copy_tags_to_snapshot
  replicate_source_db     = var.rds_replicate_source_db
  availability_zone       = var.rds_mysql_availability_zone
  backup_retention_period = var.rds_mysql_backup_retention_period
  backup_window           = var.rds_mysql_backup_window
  maintenance_window      = var.rds_mysql_maintenance_window
  iops                    = var.rds_mysql_iops
  multi_az                = var.rds_mysql_multi_az
  port                    = var.rds_mysql_port
  publicly_accessible     = var.rds_mysql_publicly_accessible

  # vpc_security_group_ids            = ["${aws_security_group.rds_mysql.id},${split(",", var.vpc_security_group_ids)}"]
  vpc_security_group_ids      = split(",", local.list_vpc_security_group_ids)
  db_subnet_group_name        = aws_db_subnet_group.rds_mysql.id
  parameter_group_name        = var.rds_mysql_parameter_group_name
  option_group_name           = var.rds_mysql_option_group_name
  storage_encrypted           = var.rds_mysql_storage_encrypted
  apply_immediately           = var.rds_mysql_apply_immediately
  allow_major_version_upgrade = var.rds_mysql_allow_major_version_upgrade
  auto_minor_version_upgrade  = var.rds_mysql_auto_minor_version_upgrade
  monitoring_role_arn         = var.rds_mysql_monitoring_role_arn
  monitoring_interval         = var.rds_mysql_monitoring_interval
  tags = merge(
    {
      "Name" = "${var.rds_mysql_identifier}"
    },
    var.rds_mysql_tags,
  )
}

