resource "aws_security_group" "rds_mysql" {
  lifecycle {
    create_before_destroy = true
  }
  name        = "${var.env}_${var.rds_mysql_sg_name}"
  description = "rds mysql security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    cidr_blocks = split(",", var.rds_mysql_allowed_ips)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = "${var.env}_${var.rds_mysql_sg_name}"
    },
    var.rds_mysql_sg_tag,
  )
}

