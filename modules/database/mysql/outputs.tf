output "address" {
  value       = aws_db_instance.rds_mysql.address
}

output "arn" {
  value       = aws_db_instance.rds_mysql.arn
}

output "id" {
  value       = aws_db_instance.rds_mysql.id
}

output "endpoint" {
  value       = aws_db_instance.rds_mysql.endpoint
}