# environment variables
variable "env" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

# rds variables
variable "rds_mysql_identifier" {
  default = "septech-rds-01"
}

variable "rds_mysql_storage" {
  default = "10"
}

variable "rds_mysql_engine" {
  default = "mysql"
}

variable "rds_mysql_engine_version" {
  default = ""
}

variable "rds_mysql_instance_class" {
  default = ""
}

variable "rds_mysql_storage_type" {
  default = "standard"
}

variable "rds_mysql_skip_final_snapshot" {
  default = "false"
}

variable "rds_mysql_copy_tags_to_snapshot" {
  default = "false"
}

variable "rds_mysql_db_name" {
  default = ""
}

variable "rds_mysql_username" {
  default = ""
}

variable "rds_mysql_password" {
  description = "password, provide through your ENV variables"
}

variable "rds_mysql_availability_zone" {
  default = ""
}

variable "rds_mysql_backup_retention_period" {
  default = "7"
}

variable "rds_mysql_backup_window" {
  default = "00:00-02:00"
}

variable "rds_mysql_maintenance_window" {
  default = "wed:15:00-wed:15:30"
}

variable "rds_mysql_iops" {
  default = 0
}

#variable "aws_region"                       {}

variable "rds_mysql_multi_az" {
  default = false
}

variable "rds_mysql_port" {
  default = "3306"
}

variable "rds_mysql_publicly_accessible" {
  default = false
}

variable "vpc_security_group_ids" {
  default = ""
}

variable "rds_mysql_option_group_name" {
  default = ""
}

variable "rds_mysql_parameter_group_name" {
  default = ""
}

# security_group variables
variable "rds_mysql_allowed_ips" {
  default = ""
}

variable "rds_mysql_sg_tag" {
  default = {}
}

variable "rds_mysql_sg_name" {
  default = "main_rds_sg"
}

variable "rds_mysql_storage_encrypted" {
  default = false
}

variable "rds_mysql_apply_immediately" {
  default = false
}

variable "rds_mysql_allow_major_version_upgrade" {
  default = false
}

variable "rds_mysql_auto_minor_version_upgrade" {
  default = true
}

variable "rds_mysql_monitoring_role_arn" {
  default = ""
}

variable "rds_mysql_monitoring_interval" {
  default = 0
}

# subnet variables
# variable "rds_mysql_subnet_name" {
#   default = "rds_mysql"
# }

variable "rds_mysql_subnet_ids" {
}

variable "rds_db_subnet_group_name" {
  default = "main_rds_subnet_group"
}

variable "rds_mysql_tags" {
  type = map(string)
  default = {
    mackerel = "No"
  }
}

variable "rds_mysql_subnet_group_tags" {
  default = {}
}

