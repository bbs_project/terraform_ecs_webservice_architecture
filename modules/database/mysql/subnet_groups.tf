#--------------------------------------------------------------
# This module creates all resources necessary for a rds_mysql
# subnet
#--------------------------------------------------------------

resource "aws_db_subnet_group" "rds_mysql" {
  name        = "${var.env}_${var.rds_db_subnet_group_name}"
  description = "${var.env} rds subnet group"
  subnet_ids  = split(",", var.rds_mysql_subnet_ids)
  tags = merge(
    {
      "Name" = "${var.env}_${var.rds_db_subnet_group_name}"
    },
    var.rds_mysql_subnet_group_tags,
  )
}

