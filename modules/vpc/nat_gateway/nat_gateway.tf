#--------------------------------------------------------------
# This module creates all resources necessary for NAT
#--------------------------------------------------------------

resource "aws_eip" "nat_gateway" {
  vpc = true

  #count                            = "${length(split(",", var.azs))}" # Comment out count to only have 1 NAT
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(
    {
      "Name" = var.nat_gateway_name
    },
    var.ip_nat_gw_tags,
  )
}

resource "aws_subnet" "nat_gateway" {
  vpc_id            = var.vpc_id
  cidr_block        = element(split(",", var.nat_gateway_subnet_cidrs), count.index)
  availability_zone = element(split(",", var.azs), count.index)
  count             = length(split(",", var.nat_gateway_subnet_cidrs))
  tags = merge(
    {
      "Name" = format("${var.nat_gateway_subnet_name}-%d", count.index)
    },
    var.nat_gw_subnet_tags,
  )
  lifecycle {
    create_before_destroy = true
  }
  map_public_ip_on_launch = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id     = element(aws_subnet.nat_gateway.*.id, count.index)
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(
    {
      "Name" = var.nat_gateway_name
    },
    var.nat_gw_tags,
  )
}

resource "aws_route_table" "nat" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.internet_gateway_id
  }
  tags = merge(
    {
      "Name" = var.nat_gateway_subnet_name
    },
    var.nat_gw_subnet_tags,
  )
}

resource "aws_route_table_association" "nat" {
  count          = length(split(",", var.nat_gateway_subnet_cidrs))
  subnet_id      = element(aws_subnet.nat_gateway.*.id, count.index)
  route_table_id = aws_route_table.nat.id
}

