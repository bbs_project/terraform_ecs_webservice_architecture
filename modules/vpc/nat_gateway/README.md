# Setup private subnet resources

- nat_gateway
- route_table
- nat_gateway_subnet

## Variables
#### Customize filed which use to config a new resource

- vpc_id: ID of VPC which the new subnet belongs to.
- nat_gateway_name: The name of NAT gateway which will be displayed on AWS console
- nat_gateway_subnet_name: The name of NAT gateway subnet which will be displayed on AWS console
- nat_gateway_subnet_cidrs: CDIR blocks
- internet_gateway_id: The id of the specific Gateway
- azs: Indicate the specific region where the new subnet available

## Outputs
#### Output parameters of the created resource which will be printed on the console

- nat_gateway_id: ID of created NAT gateway resource
