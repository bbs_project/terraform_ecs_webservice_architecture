output "nat_gateway_id" {
  value = join(",", aws_nat_gateway.nat.*.id)
}

