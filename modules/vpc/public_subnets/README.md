# Setup private subnet resources

- private_subnet
- private_route_table

## Variables
#### Customize filed which use to config a new resource

- vpc_id: ID of VPC which the new subnet belongs to.
- private_subnet_cidrs: CDIR blocks
- azs: Indicate the specific region where the new subnet available.
- private_subnet_name: The name of resource which will be displayed on AWS console
- nat_gateway_id: ID of related NAT gateway

## Outputs
#### Output parameters of the created resource which will be printed on the console

- private_subnet_ids: ID of created subnet resource
