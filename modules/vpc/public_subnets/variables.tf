variable "vpc_id" {
  default = ""
}

variable "gateway_id" {
  default = ""
}

variable "public_subnet_name" {
  default = "public_subnet_name"
}

variable "public_subnet_cidrs" {
  default = "10.0.2.0/24,10.0.4.0/24"
}

variable "azs" {
  default = "ap-northeast-1a,ap-northeast-1c"
}

variable "public_subnet_tags" {
  default = {}
}

