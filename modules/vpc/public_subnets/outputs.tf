output "public_subnet_ids" {
  value = join(",", aws_subnet.public.*.id)
}

output "subnet_ids" {
  value = aws_subnet.public.*.id
}