output "private_subnet_ids" {
  value = join(",", aws_subnet.private.*.id)
}

output "subnet_ids" {
  value = aws_subnet.private.*.id
}
