variable "vpc_id" {
  default = ""
}

variable "instance_id" {
  default = ""
}

variable "private_subnet_name" {
  default = "private"
}

variable "private_subnet_cidrs" {
  default = "10.0.3.0/24,10.0.5.0/24"
}

variable "azs" {
  default = "ap-northeast-1a,ap-northeast-1c"
}

variable "private_subnet_tags" {
  default = {}
}

