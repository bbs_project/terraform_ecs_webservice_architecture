resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.s3_bucket_name
  acl    = "private"

  tags = {
    Name         = var.s3_bucket_name
    Company      = "FV"
    ProductGroup = "CTX"
  }
}

resource "aws_s3_bucket" "tech-asset-storage-dev" {
  bucket = "tech-asset-storage-dev"
  acl    = "public-read"
  policy = "${file("dev-policy.json")}"

  tags = {
    Name         = "tech-asset-storage-dev"
    Company      = "FV"
    Feature      = "CTX - Share"
    ProductGroup = "CTX"
  }
}

resource "aws_s3_bucket" "tech-asset-storage-staging" {
  bucket = "tech-asset-storage-staging"
  acl    = "public-read"
  policy = "${file("stag-policy.json")}"

  tags = {
    Name         = "tech-asset-storage-staging"
    Company      = "FV"
    ProductGroup = "CTX"
    Feature      = "CTX - Share"
  }
}

resource "aws_s3_bucket" "tech-asset-storage-prod" {
  bucket = "tech-asset-storage-prod"
  acl    = "public-read"
  policy = "${file("prod-policy.json")}"

  tags = {
    Name         = "tech-asset-storage-prod"
    Company      = "FV"
    Feature      = "CTX - Share"
    ProductGroup = "CTX"
  }

  lifecycle_rule {
    abort_incomplete_multipart_upload_days = 0
    enabled                                = true
    id                                     = "convert_object_to_inteligent_tiering"
    tags                                   = {}
    transition {
      days          = 1
      storage_class = "INTELLIGENT_TIERING"
    }
  }
}
