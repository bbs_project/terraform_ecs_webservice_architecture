resource "aws_nat_gateway" "gw" {

  allocation_id = "eipalloc-0350d3d955739ce73"
  subnet_id     = "subnet-0f465530734c300b0"
  tags = {
    Name = "project-name_vpc"
  }
}
