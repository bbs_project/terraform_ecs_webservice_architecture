output "vpc_id" {
  value = module.infra_vpc.vpc_id
}

output "vpc_cidr" {
  value = module.infra_vpc.vpc_cidr
}

output "gateway_id" {
  value = module.infra_vpc.gateway_id
}

output "zone_id" {
  value = aws_route53_zone.private.zone_id
}