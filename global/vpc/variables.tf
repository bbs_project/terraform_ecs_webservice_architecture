# common variables
variable "region" {
  default = "ap-northeast-1"
}

variable "azs" {
  default = "ap-northeast-1a,ap-northeast-1c"
}

# vpc_base module variables
variable "vpc_name" {
  default = "vpc"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

