module "infra_vpc" {
  source   = "../../modules/vpc/base"
  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
}

resource "aws_route53_zone" "private" {
  name = "project-name.vpc"

  vpc {
    vpc_id = module.infra_vpc.vpc_id
  }
}
