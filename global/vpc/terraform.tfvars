# common variables
region = "ap-northeast-1"
azs    = "ap-northeast-1a,ap-northeast-1c"

# vpc/base module variables
vpc_name = "project-name_vpc"
vpc_cidr = "10.36.0.0/16"
