# Create a Route53 zone
resource "aws_route53_zone" "project-name" {
  name = "project-name.example.net"
}

# Create a CNAME record for the main domain
resource "aws_route53_record" "prd-project-name" {
  zone_id = aws_route53_zone.project-name.zone_id
  alias {
    evaluate_target_health = "false"
    name                   = "dualstack.prod-alb-project-name-276577934.ap-northeast-1.elb.amazonaws.com"
    zone_id                = "Z14GRHDCWA56QT"
  }

  name = "project-name.example.net"
  type = "A"
}

# Create an "project-name" subdomain record
resource "aws_route53_record" "dev-project-name" {
  zone_id = aws_route53_zone.project-name.zone_id
  name    = "dev"
  type    = "CNAME"
  ttl     = "300"
  records = ["dev-alb-project-name-430535641.ap-northeast-1.elb.amazonaws.com"] # Replace with the desired target record(s) for the subdomain
}

# Create an "project-name" subdomain record
resource "aws_route53_record" "stg-project-name" {
  zone_id = aws_route53_zone.project-name.zone_id
  name    = "stg"
  type    = "CNAME"
  ttl     = "300"
  records = ["Stag-alb-project-name-592223547.ap-northeast-1.elb.amazonaws.com"] # Replace with the desired target record(s) for the subdomain
}

resource "aws_route53_zone" "ctx" {
  name = "ctx.example.net"
}

# Create a CNAME record for the main domain
resource "aws_route53_record" "prd-ctx" {
  zone_id = aws_route53_zone.ctx.zone_id
  alias {
    evaluate_target_health = "false"
    name                   = "d18b6u6lom3ul0.cloudfront.net"
    zone_id                = "Z2FDTNDATAQYW2"
  }

  name = "ctx.example.net"
  type = "A"
}

# Create an "project-name" subdomain record
resource "aws_route53_record" "dev-ctx" {
  zone_id = aws_route53_zone.ctx.zone_id
  name    = "dev"
  type    = "CNAME"
  ttl     = "300"
  records = ["dv9echwp9s10b.cloudfront.net"] # Replace with the desired target record(s) for the subdomain
}


# Create an "project-name" subdomain record
resource "aws_route53_record" "stg-ctx" {
  zone_id = aws_route53_zone.ctx.zone_id
  name    = "stg"
  type    = "CNAME"
  ttl     = "300"
  records = ["d24u77my3fo7h2.cloudfront.net"] # Replace with the desired target record(s) for the subdomain
}


