output "name" {
  value = "${aws_iam_role.ecs_events.name}"
}

output "arn" {
  value = "${aws_iam_role.ecs_events.arn}"
}

output "task_role_arn" {
  value = "${aws_iam_role.ecs-tasks.arn}"
}