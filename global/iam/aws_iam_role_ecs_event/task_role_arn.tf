data "aws_iam_policy_document" "ecs-tasks" {
	statement {
		effect  = "Allow"
		actions = ["sts:AssumeRole"]

		principals {
  			type        = "Service"
  			identifiers = ["ecs-tasks.amazonaws.com"]
		}
	}
}

data "aws_iam_policy_document" "ecr-policy" {
	statement {
		effect = "Allow"

		actions = [
                "ecr:DescribeImageScanFindings",
                "ecr:GetLifecyclePolicyPreview",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetAuthorizationToken",
                "ecr:ListTagsForResource",
                "ecr:ListImages",
                "ecr:UntagResource",
                "ecr:BatchGetImage",
                "ecr:DescribeImages",
                "ecr:TagResource",
                "ecr:DescribeRepositories",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetRepositoryPolicy",
                "ecr:GetLifecyclePolicy"
		]

		resources = ["*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "logs:*"
		]

		resources = ["*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "ssm:DescribeParameters"
		]

		resources = ["*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "ssm:GetParameters"
		]

		resources = ["arn:aws:ssm:ap-northeast-1:952866727984:parameter/RC_*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "kms:Decrypt"
		]

		resources = ["arn:aws:kms:ap-northeast-1:952866727984:key/e0187fbf-1ee9-4884-8f76-7a796cdf29b0"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "iam:PassRole"
		]

		resources = ["*"]
	}
}

resource "aws_iam_role" "ecs-tasks" {
  name = var.task_role_name

  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
}

resource "aws_iam_role_policy" "ecr-policy" {
  name = var.task_role_name
  role = aws_iam_role.ecs-tasks.id

  policy = data.aws_iam_policy_document.ecr-policy.json
}
