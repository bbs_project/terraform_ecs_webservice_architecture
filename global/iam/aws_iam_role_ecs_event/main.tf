data "aws_iam_policy_document" "ecs_events" {
	statement {
		effect  = "Allow"
		actions = ["sts:AssumeRole"]

		principals {
  			type        = "Service"
  			identifiers = ["events.amazonaws.com"]
		}
	}
}

data "aws_iam_policy_document" "ecs_events_run_task" {
	statement {
		effect = "Allow"

		actions = [
  			    "ecs:RunTask"
		]

		resources = ["*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "logs:*"
		]

		resources = ["*"]
	}

	statement {
		effect = "Allow"

		actions = [
  			    "iam:PassRole"
		]

		resources = ["*"]
	}
}

resource "aws_iam_role" "ecs_events" {
  name = var.role_name

  assume_role_policy = data.aws_iam_policy_document.ecs_events.json
}

resource "aws_iam_role_policy" "ecs_events_run_task" {
  name = var.role_name
  role = aws_iam_role.ecs_events.id

  policy = data.aws_iam_policy_document.ecs_events_run_task.json
}
