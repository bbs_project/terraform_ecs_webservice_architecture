variable "role_name" {
  default = "ecs_events_role_regulation_check"
}

variable "task_role_name" {
  default = "ecs_task_role_regulation_check"
}