resource "aws_iam_group_membership" "qas" {
  name = "project-name-group-qas-membership"

  users = [
    "chau_hq@septeni-technology.jp",
    "hanh_lm@septeni-technology.jp",
    "hiroaki.kan@septeni-original.co.jp",
    "to_maruyama@septeni-original.co.jp",
    "ryota.takagi@septeni-original.co.jp",
    "y_fujiwara@septeni-original.co.jp",
    "masatoshi.oka@septeni-original.co.jp",
    "tuoi_tt@septeni-technology.jp",
    "hue_nth@septeni-technology.jp"
  ]

  group = aws_iam_group.qas.name
}

resource "aws_iam_group" "qas" {
  name = "project-name-tech-qas"
  path = "/users/"
}

resource "aws_iam_group_policy" "qas" {
  name  = "project-name-tech-qas-policy"
  group = aws_iam_group.qas.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:iam::952866727984:role/Septech-QAs"
    }
  ]
}
EOF
}
