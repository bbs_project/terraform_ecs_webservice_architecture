resource "aws_iam_group_membership" "devs" {
  name = "project-name-group-membership"

  users = [
    "hung_pt@septeni-technology.jp",
    "long_tv@septeni-technology.jp",
    "tuan_ta@septeni-technology.jp",
    "hung_dt@septeni-technology.jp",
    "quanganh_v@septeni-technology.jp",
    "trung_dd@septeni-technology.jp",
    "lam_dt@septeni-technology.jp",
    "anh_pl@septeni-technology.jp",
    "hieu_nm@septeni-technology.jp",
    "hoai_pt@septeni-technology.jp"
  ]

  group = aws_iam_group.devs.name
}
resource "aws_iam_group" "devs" {
  name = "project-name-tech-developers"
  path = "/users/"
}

resource "aws_iam_group_policy" "devs" {
  name  = "project-name-tech-policy"
  group = aws_iam_group.devs.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:iam::952866727984:role/Septech-Developers"
    }
  ]
}
EOF
}
