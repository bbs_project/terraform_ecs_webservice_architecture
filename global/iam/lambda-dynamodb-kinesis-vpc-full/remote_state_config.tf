terraform {
  backend "s3" {
    bucket                  = "project-name-terraform-state-file-storage"
    key                     = "global/iam/lambda-dynamodb-kinesis-vpc-full/terraform.tfstate"
    region                  = "ap-northeast-1"
    encrypt                 = true
    shared_credentials_file = "~/.aws/config"
    profile                 = "project-name"
  }
}
