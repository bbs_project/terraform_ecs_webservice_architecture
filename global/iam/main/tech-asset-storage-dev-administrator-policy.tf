data "aws_iam_policy_document" "tech-asset-storage-dev" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:ListBucketVersions",
      "s3:ListBucket",
      "s3:GetBucketVersioning",
      "s3:GetBucketLocation",
    ]

    resources = ["arn:aws:s3:::*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObjectAcl",
      "s3:PutObject",
      "s3:GetObjectVersion",
      "s3:GetObjectAcl",
      "s3:GetObject",
      "s3:DeleteObject",
    ]

    resources = ["arn:aws:s3:::tech-asset-storage-dev/*",
      "arn:aws:s3:::tech-asset-storage-dev",
    ]
  }
}

resource "aws_iam_policy" "tech-asset-storage-dev-administrator-policy" {
  name   = "tech-asset-storage-dev-administrator-policy"
  policy = "${data.aws_iam_policy_document.tech-asset-storage-dev.json}"
}
