data "aws_iam_policy_document" "tech-asset-storage-prod-apps" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
    ]

    resources = ["arn:aws:s3:::*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
      "s3:PutObject",
      "s3:GetObject",
    ]

    resources = ["arn:aws:s3:::tech-asset-storage-prod/*",
      "arn:aws:s3:::tech-asset-storage-prod",
    ]
  }
}

resource "aws_iam_policy" "tech-asset-storage-prod-application-policy" {
  name   = "tech-asset-storage-prod-application-policy"
  policy = "${data.aws_iam_policy_document.tech-asset-storage-prod-apps.json}"
}
