data "aws_iam_policy_document" "qas_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = ["arn:aws:s3:::*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucketVersions",
      "s3:ListBucket",
      "s3:GetBucketVersioning",
      "s3:GetBucketLocation",
      "s3:GetObjectVersion",
      "s3:GetObjectAcl",
      "s3:GetObject"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "qas" {
  name        = "iam-policy-project-name-qas-terraform"
  path        = "/"
  description = "project-name QAs policy"
  policy      = data.aws_iam_policy_document.qas_policy.json

  lifecycle {
    create_before_destroy = true
  }

}

data "aws_iam_policy_document" "qas_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::267498347058:root"]
    }
  }
}

resource "aws_iam_role" "qas" {
  name               = "Septech-QAs"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.qas_assume_policy.json
}


resource "aws_iam_role_policy_attachment" "qas_policy_attach" {
  role       = "Septech-QAs"
  policy_arn = aws_iam_policy.qas.arn

  lifecycle {
    create_before_destroy = true
  }

}
