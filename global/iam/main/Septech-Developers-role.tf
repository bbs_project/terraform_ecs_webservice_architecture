data "aws_iam_policy_document" "devs_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:*",
      "ec2:*",
      "elasticloadbalancing:*",
      "rds:*",
      "ecr:*",
      "ecs:*",
      "events:*",
      "lambda:*",
      "logs:*",
      "sqs:*",
      "sns:*",
      "ses:*",
      "tag:*",
      "kinesis:*",
      "dynamodb:*",
      "cloudwatch:*",
      "cloudformation:*",
      "apigateway:*",
      "cloudfront:*"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "iam:PassRole",
      "iam:CreateServiceLinkedRole",
      "iam:DeleteServiceLinkedRole",
      "iam:ListRoles",
      "iam:GetRole",
      "organizations:DescribeOrganization",
      "account:ListRegions"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:DescribeAssociation",
      "ssm:GetDeployablePatchSnapshotForInstance",
      "ssm:GetDocument",
      "ssm:DescribeDocument",
      "ssm:GetManifest",
      "ssm:GetParameter",
      "ssm:GetParameters",
      "ssm:ListAssociations",
      "ssm:ListInstanceAssociations",
      "ssm:PutInventory",
      "ssm:PutComplianceItems",
      "ssm:PutConfigurePackageResult",
      "ssm:UpdateAssociationStatus",
      "ssm:UpdateInstanceAssociationStatus",
      "ssm:UpdateInstanceInformation",
      "ssm:Describe*",
      "ssm:Get*"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    not_actions = [
      "iam:*",
      "organizations:*",
      "account:*"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2messages:AcknowledgeMessage",
      "ec2messages:DeleteMessage",
      "ec2messages:FailMessage",
      "ec2messages:GetEndpoint",
      "ec2messages:GetMessages",
      "ec2messages:SendReply"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Deny"

    actions = [
      "ec2:PurchaseReservedInstancesOffering",
      "rds:PurchaseReservedDBInstancesOffering",
      "redshift:PurchaseReservedNodeOffering",
      "route53domains:TransferDomain",
      "route53domains:RegisterDomain",
      "ec2:Delete*",
      "s3:DeleteBucket*"
    ]

    resources = ["*"]
  }
}
resource "aws_iam_policy" "devs" {
  name        = "iam-policy-project-name-devs-terraform"
  path        = "/"
  description = "project-name Devs policy"
  policy      = data.aws_iam_policy_document.devs_policy.json

  lifecycle {
    create_before_destroy = true
  }

}

data "aws_iam_policy_document" "devs_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::267498347058:root"]
    }
  }
}

resource "aws_iam_role" "devs" {
  name               = "Septech-Developers"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.devs_assume_policy.json
}


resource "aws_iam_role_policy_attachment" "devs_policy_attach" {
  role       = "Septech-Developers"
  policy_arn = aws_iam_policy.devs.arn

  lifecycle {
    create_before_destroy = true
  }

}
